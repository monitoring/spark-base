FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

LABEL maintainer="it-dep-da-asm@cern.ch"

COPY sbt.repo /etc/yum.repos.d/

RUN yum -y install java-11-openjdk \
    && yum -y install sbt \
    && yum clean all

