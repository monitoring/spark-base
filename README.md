Available tags:

- **2024-08-08** Use latest Alma9 image, with Java 11 and sbt, use Python 3 in the Alma9 image
- **2024-03-20:** Use latest CentOS7 image, use Java 11, install sbt, install python3, don't install mesos  
- **2017-05-18:** Switch to Java 8
- **2016-06-24:** Added Mesos to support running on our Mesos cluster
- **2016-06-17:** Added Python libs and pip for pyspark support
- **2016-06-16:** First version, contains Java 8 and Kerberos with config

Build and push image:
```
docker login gitlab-registry.cern.ch
docker build -t gitlab-registry.cern.ch/monitoring/spark-base:20240808 .
docker push gitlab-registry.cern.ch/monitoring/spark-base:20240808
```
